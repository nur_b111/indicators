var express = require('express'),
    app = express(),
    port = 3000,
    mongoose = require('mongoose');
var indicators = require('./routes/indicators');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/node-test')
    .then(()=> console.log('Connection succesful'))
    .catch(()=> console.error(err));

// app.use('/', (req, res) => {
//   console.log('Запрос на сервер');  
//   res.send('Wellcome!');
// });
app.use(express.static(__dirname + '/public'));
app.set('view engine', 'ejs');
app.use('/indicators', indicators);

app.listen(port, () => {
    console.log('Server listening on port ' + port);
});