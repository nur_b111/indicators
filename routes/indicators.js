var express = require('express'),
    router = express.Router(),
    indicator = require("../controllers/indicatorController.js");

//Получение всех индикаторов из БД
router.get('/', indicator.list);


module.exports = router;