const mongoose = require('mongoose');

let Indicator = mongoose.Schema({
    title: { type:String },
    value: { type:Number },
    percentValue:{ type: Number},
    minValue: { type: Number },
    maxValue: { type:Number }
});

module.exports = mongoose.model('Indicator', Indicator);