const Indicator = require('../models/indicator.js');
var mongoose = require("mongoose"),
    indicatorController = {};

indicatorController.list = function(req, res) {
    mongoose.connection.db.listCollections({name: 'indicators'})
    .next(function(err, collinfo) {
        if (collinfo) {
            Indicator.find({}).exec(function (err, response) {
                if (err) {
                    console.log("Error:", err);
                }
                else {
                    console.log('Result: '+ response.length);
                    res.render("../views/index", {indicators: response});    
                }
            }) 
        } else {
            createIndicators(8, (err, result)=>{
                Indicator.find({}).exec(function (err, response) {
                    if (err) {
                        console.log("Error:", err);
                    }
                    else {
                        console.log('Result: '+ response.length);
                        res.render("../views/index", {indicators: response});    
                    }
                })                                                       
            });
        }
    });
};

//Скрипт для добавления временных данных в монгу, если БД не существует
function createIndicators(length,callback){
    var index=1;
    for(let i = 0; i<length; i++){
        let title = 'Иникатор № '+ (i+1),
            max = 0,
            min = 1000,
            value=Math.floor(Math.random() * (max - min)) + min;
        let indicator = new Indicator({
            title: title,
            value: value,
            minValue: 0,
            maxValue: 1000,
            percentValue: value/10
        });
        indicator
            .save()
            .then(() => {
                index++;
                console.log('Created');
                if(index==length) return callback("All done");                
            })
            .catch(err => {
                 return callback(new Error('An error has occured'));
            });
    };
}

module.exports = indicatorController;